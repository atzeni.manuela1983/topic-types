# Build a car

Prerequisites:

- [Tires](../tires/index.md)
- A crane
- An [engine](../engine/index.md)
- [Seats](../seats/index.md)

To build a car:

1. Grab your tires and set them upright.
1. Turn on the crane.
1. Use the crane to lift the engine 10 feet up in the air.
1. Put the engine under the hood.
1. Behind the engine, add the seats.

You can now drive the car.