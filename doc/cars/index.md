# Cars (landing page)

A car is a machine you use to get from one place to another.

You sit in the machine and drive it by pressing pedals on the floor.
You change direction by turning a wheel that's in front of your seat.

A car is similar to a train, but the machine is not attached
to rails. Instead, a car usually goes on roads paved in asphalt. If you are in high school,
you can take the car off the road and drive across your friend's front yard.
If they are a good friend, they will laugh and not call the police.

A car can have a variety of shapes, but usually it's about ten feet long by
four feet wide and five feet tall.

To move, a car needs an engine full of gasoline.

Cars are smaller than trucks.

Cars include:

- [Tires](../tires/index.md)
- [Engine](../engine/index.md)
- [Seats](../seats/index.md)
- [Fabric](../fabrics/index.md)
