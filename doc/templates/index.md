# Topic types

The topic types are covered in detail here: [https://docs.gitlab.com/ee/development/documentation/structure.html](https://docs.gitlab.com/ee/development/documentation/structure.html)

However, this page also includes an overview of each type.

- [Landing page](#landing-pages)
- [Concept](#concept)
- [Task](#task)
- [Reference](#reference)
- [Troubleshooting](#troubleshooting)

GitLab also uses high-level landing pages.

## Landing pages

Landing topics are topics that group other topics.

Users who are using the in-product help do not have a left nav,
and need these topics to navigate the documentation.

These topics can also help other users find the most important topics
in a section.

Landing page topics should be in this format:

```markdown
# Title (a noun, like "CI/CD or "Analytics")

Brief introduction to the concept or product area.
Include the reason why someone would use this thing.

## Related topics

- Bulleted list of
- Important related topics
```

## Concept

A concept topic introduces a single feature or concept.

A concept should answer the questions:

- What is this?
- Why would I use it?

Think of everything someone might want to know if they’ve never heard of this word before.

Don’t tell them **how** to do this thing. Tell them **what it is**.

If you start describing about another topic, start a new concept and link to it.

Concept topics should be in this format:

```markdown
# Title (a noun, like "Widgets")

A paragraph that explains what this thing is.

Another paragraph that explains what this thing is.

Remember, if you start talking about another concept, stop yourself.
Each concept topic should be about one concept only.
```

## Task

A task topic gives instructions for how to complete a procedure.

Task topics should be in this format:

```markdown
# Title (starts with an active verb, like "Create a widget" or "Delete a widget")

Do this task when you want to...

Prerequisites (optional):

- Thing 1
- Thing 2
- Thing 3

To do this task:

1. Location then action. (Go to this menu, then select this item.)
1. Another step.
1. Another step.

Task result (optional). Next steps (optional).
```

Here is an example.

```markdown
# Create an issue

Create an issue when you want to track bugs or future work.

Prerequisites:

- A minimum of Contributor access to a project in GitLab.

To create an issue:

1. Go to **Issues > List**.
1. In the top right, click **New issue**.
1. Complete the fields. (If you have a reference topic that lists each field, link to it here.)
1. Click **Submit issue**.

The issue is created. You can view it by going to **Issues > List**.
```

## Reference

A reference topic provides information in an easily-scannable format,
like a table or list. It's similar to a dictionary or encyclopedia entry.

```markdown
# Title (a noun, like "Pipeline settings" or "Administrator options")

Introductory sentence.

| Setting | Description |
|---------|-------------|
| **Name** | Descriptive sentence about the setting. |
```

## Troubleshooting

Troubleshooting topics can be one of two categories:

- **Troubleshooting task.** This topic is written the same as a [standard task topic](#task).
  For example, "Run debug tools" or "Verify syntax."
- **Troubleshooting reference.** This topic has a specific format.

Troubleshooting reference topics should be in this format:

```markdown
# Title (the error message or a description of it)

You might get an error that states <error message>.

This issue occurs when...

The workaround is...
```
