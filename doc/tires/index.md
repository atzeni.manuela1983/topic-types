# Tires (concept)

Tires are round rubber disks that you put under the car.

Use tires when you need to move forward or backward or turn.

You can use two tires but we recommend you use four.

You should check your tires every six months to ensure they are still valid.

## Create a tire (task)

Create a tire when you need your car to move.

Prerequisites:

- Rubber
- Some kind of knife
- A furnace

To create a tire:

1. Go to your furnace and open the lid.
1. Put the rubber in the furnace until it melts.
1. Take the rubber out and pour it into the mold.
1. When the mold is cool to the touch, remove the tire.

You can now put the tire on the car.

## Types of tires (reference)

| Type | Description |
|------|-------------|
| Radial | A tire that's good for everyday use. |
| All-wheel-drive | A tire that's good for driving over rough surfaces. |

## Brands of tires (reference)

| Brand | Description |
|------|-------------|
| Michelin | GitLab recommends these tires. The logo is a man made from tires. |
| Firestone | These tires are also good, especially if you need to drive in the snow. The logo is fire. |

## Troubleshooting tires (troubleshooting section head)

When working with tires, you may have issues you need to troubleshoot.

### Test the tire against the template (troubleshooting task)

When the tire does not fit properly on the car, test the tire against the template.

1. From the drawer, remove the mold.
1. Open the mold and place the tire inside.
1. If the tire does not fit snugly, place the tire back in the furnace and complete
   the steps to [create a tire](#create-a-tire).

### `Error: tire has hole in it` (troubleshooting reference)

You may get an error that says, `Error: tire has hole in it`.

This issue occurs when an object has punctured your tire and air
is escaping like prisoners from Alcatraz.

To work around this issue:

1. Go to the local hardware store.
1. Buy a patch kit.
1. Apply the patch kit.

After an hour you can put the tire back on the car.
