# Seats (concept)

Seats are metal objects that are shaped like an "L."

You bend at the waist to sit in them.

Two seats are parallel, facing forward, in front of the steering wheel.
(Don't define the steering wheel here! Link to it.)

Seats are covered in [fabric](../fabrics/index.md).

## Create a seat (task)

Create a seat when you want to allow users to sit in the car.

Prerequisites:

- A five-foot-long chunk of metal
- Fabric
- Strength

To create a seat:

1. Bend the metal into an "L" shape.
1. Cut the fabric into a size and shape that fits the bent metal.
1. Sew the fabric.
1. Put the fabric over the metal and secure it.

## Destroy a seat (task)

Destroy a seat when you no longer need it.

To destroy a seat:

1. Put the seats in a garage and cover them with ketchup.
1. Open the garage door at night.

The raccoons should have a good time. After a few nights, take the seats to the local dump.
