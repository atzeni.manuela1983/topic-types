# Engines (concept)

Engines are machines inside the car. They generate the power that makes the car go.

You put oil in them to keep them from getting dry.

The engine also needs a constant influx of gas. The gas explodes into mist.
These explosions cause cylinders to go up and down, and power is generated.
(Don't quote me on this.)

The bigger the engine:

- The louder it is.
- The scarier it is when my grandma is driving.

## Create an engine (task)

Create an engine when you need to send power to a car.

Prerequisites:

- Metal
- Tools

To create an engine:

1. Mold the metal.
1. Put it together.
1. Hook it up to some gas.
1. Hope it does something.

The engine is complete. Now you can put the engine in the car.

## Destroy an engine (task)

To destroy an engine:

1. Call Freddy Krueger.
1. Let him go wild with a torch.

## Types of engines (reference)

| Type | Description |
|------|-------------|
| V6 | An engine. |
| V8 | A bigger engine. |

## Brands of engines (reference)

| Brand | Description |
|------|-------------|
| Marcel special | An engine. |
| Axil mega | Brand 2. |
